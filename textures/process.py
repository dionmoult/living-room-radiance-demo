#!/usr/bin/python

import sys

input_filename = sys.argv[1]
base_filename = ''.join(input_filename.split('.')[0:-1])
x_filename = base_filename + '_x.dat'
y_filename = base_filename + '_y.dat'
z_filename = base_filename + '_z.dat'

with open(x_filename, 'w') as x_file:
    with open(y_filename, 'w') as y_file:
        with open(z_filename, 'w') as z_file:
            with open(input_filename, 'r') as input_file:
                is_pixel_line = False
                for line in input_file:
                    if 'Y' in line and 'X' in line:
                        for output in [x_file, y_file, z_file]:
                            output.write('2\n')
                            output.write('0 1 ' + line.split(' ')[1] + '\n')
                            output.write('0 1 ' + line.split(' ')[3] + '\n\n')
                        is_pixel_line = True
                        continue
                    if not is_pixel_line:
                        continue
                    data = line.split()
                    if data[0] == '0':
                        for output in [x_file, y_file, z_file]:
                            output.write('\n')
                    x_file.write(data[2] + ' ')
                    y_file.write(data[3] + ' ')
                    z_file.write(data[4] + ' ')
