lib/scene.rtm: blender/scene.obj mat/scene.mat
	obj2mesh -a mat/scene.mat blender/scene.obj > lib/scene.rtm

lib/wall.rtm: blender/wall.obj mat/scene.mat
	obj2mesh -a mat/scene.mat blender/wall.obj > lib/wall.rtm

lib/keyboard_filco_majestouch_2_tenkeyless.rtm: blender/keyboard_filco_majestouch_2_tenkeyless.obj mat/keyboard_filco_majestouch_2_tenkeyless.mat
	obj2mesh -a mat/keyboard_filco_majestouch_2_tenkeyless.mat blender/keyboard_filco_majestouch_2_tenkeyless.obj > lib/keyboard_filco_majestouch_2_tenkeyless.rtm

lib/lamp_ikea_ingared_beige.rtm: blender/lamp_ikea_ingared_beige.obj mat/lamp_ikea_ingared_beige.mat
	obj2mesh -a mat/lamp_ikea_ingared_beige.mat blender/lamp_ikea_ingared_beige.obj > lib/lamp_ikea_ingared_beige.rtm

lib/monitor_lg_24m38h.rtm: blender/monitor_lg_24m38h.obj mat/monitor_lg_24m38h.mat
	obj2mesh -a mat/monitor_lg_24m38h.mat blender/monitor_lg_24m38h.obj > lib/monitor_lg_24m38h.rtm

lib/mouse_microsoft_intellimouse_optical_usb.rtm: blender/mouse_microsoft_intellimouse_optical_usb.obj mat/mouse_microsoft_intellimouse_optical_usb.mat
	obj2mesh -a mat/mouse_microsoft_intellimouse_optical_usb.mat blender/mouse_microsoft_intellimouse_optical_usb.obj > lib/mouse_microsoft_intellimouse_optical_usb.rtm

lib/mousepad_generic.rtm: blender/mousepad_generic.obj mat/mousepad_generic.mat
	obj2mesh -a mat/mousepad_generic.mat blender/mousepad_generic.obj > lib/mousepad_generic.rtm

lib/book_rendering_with_radiance.rtm: blender/book_rendering_with_radiance.obj mat/book_rendering_with_radiance.mat
	obj2mesh -a mat/book_rendering_with_radiance.mat blender/book_rendering_with_radiance.obj > lib/book_rendering_with_radiance.rtm

lib/computer_em035_mecer_prelude_micro_atx_tower.rtm: blender/computer_em035_mecer_prelude_micro_atx_tower.obj mat/computer_em035_mecer_prelude_micro_atx_tower.mat
	obj2mesh -a mat/computer_em035_mecer_prelude_micro_atx_tower.mat blender/computer_em035_mecer_prelude_micro_atx_tower.obj > lib/computer_em035_mecer_prelude_micro_atx_tower.rtm

lib/earphones_sennheiser_m2_ieg.rtm: blender/earphones_sennheiser_m2_ieg.obj mat/earphones_sennheiser_m2_ieg.mat
	obj2mesh -a mat/earphones_sennheiser_m2_ieg.mat blender/earphones_sennheiser_m2_ieg.obj > lib/earphones_sennheiser_m2_ieg.rtm

lib/sconce_generic_sphere.rtm: blender/sconce_generic_sphere.obj mat/sconce_generic_sphere.mat
	obj2mesh -a mat/sconce_generic_sphere.mat blender/sconce_generic_sphere.obj > lib/sconce_generic_sphere.rtm

lib/door_double_sliding.rtm: blender/door_double_sliding.obj mat/door_double_sliding.mat
	obj2mesh -a mat/door_double_sliding.mat blender/door_double_sliding.obj > lib/door_double_sliding.rtm

lib/switch_generic.rtm: blender/switch_generic.obj mat/switch_generic.mat
	obj2mesh -a mat/switch_generic.mat blender/switch_generic.obj > lib/switch_generic.rtm

lib/door_single_820.rtm: blender/door_single_820.obj mat/door_single_820.mat
	obj2mesh -a mat/door_single_820.mat blender/door_single_820.obj > lib/door_single_820.rtm

lib/window_generic.rtm: blender/window_generic.obj mat/window_generic.mat
	obj2mesh -a mat/window_generic.mat blender/window_generic.obj > lib/window_generic.rtm

lib/lamp_floor_generic.rtm: blender/lamp_floor_generic.obj mat/lamp_floor_generic.mat
	obj2mesh -a mat/lamp_floor_generic.mat blender/lamp_floor_generic.obj > lib/lamp_floor_generic.rtm

lib/shoe_rack_generic.rtm: blender/shoe_rack_generic.obj mat/shoe_rack_generic.mat
	obj2mesh -a mat/shoe_rack_generic.mat blender/shoe_rack_generic.obj > lib/shoe_rack_generic.rtm

lib/switch_toggle_generic.rtm: blender/switch_toggle_generic.obj mat/switch_toggle_generic.mat
	obj2mesh -a mat/switch_toggle_generic.mat blender/switch_toggle_generic.obj > lib/switch_toggle_generic.rtm

lib/gpo_double.rtm: blender/gpo_double.obj mat/gpo_double.mat
	obj2mesh -a mat/gpo_double.mat blender/gpo_double.obj > lib/gpo_double.rtm

lib/plug_generic.rtm: blender/plug_generic.obj mat/plug_generic.mat
	obj2mesh -a mat/plug_generic.mat blender/plug_generic.obj > lib/plug_generic.rtm

lib/context.rtm: lib/context/obj/context.obj lib/context/mat/context.mat lib/chlorophytum-comosum/mat/chlorophytum-comosum.mat lib/crassula-multicava/mat/crassula-multicava.mat lib/nephrolepis-exaltata/mat/nephrolepis-exaltata.mat lib/tradescantia-pallida/mat/tradescantia-pallida.mat lib/vinca-major-variegata/mat/vinca-major-variegata.mat
	obj2mesh \
	-a lib/context/mat/context.mat \
	-a lib/chlorophytum-comosum/mat/chlorophytum-comosum.mat \
	-a lib/crassula-multicava/mat/crassula-multicava.mat \
	-a lib/nephrolepis-exaltata/mat/nephrolepis-exaltata.mat \
	-a lib/tradescantia-pallida/mat/tradescantia-pallida.mat \
	-a lib/vinca-major-variegata/mat/vinca-major-variegata.mat \
	lib/context/obj/context.obj > lib/context.rtm

.PHONY : view
view: lib/scene.rtm lib/wall.rtm lib/keyboard_filco_majestouch_2_tenkeyless.rtm lib/lamp_ikea_ingared_beige.rtm lib/monitor_lg_24m38h.rtm lib/mouse_microsoft_intellimouse_optical_usb.rtm lib/mousepad_generic.rtm lib/book_rendering_with_radiance.rtm lib/computer_em035_mecer_prelude_micro_atx_tower.rtm lib/earphones_sennheiser_m2_ieg.rtm lib/sconce_generic_sphere.rtm lib/door_double_sliding.rtm lib/switch_generic.rtm lib/door_single_820.rtm lib/window_generic.rtm lib/lamp_floor_generic.rtm lib/shoe_rack_generic.rtm lib/switch_toggle_generic.rtm lib/gpo_double.rtm lib/plug_generic.rtm lib/context.rtm
	rad -o x11 -N 12 scene.rif

.PHONY : render
render: lib/scene.rtm lib/wall.rtm lib/keyboard_filco_majestouch_2_tenkeyless.rtm lib/lamp_ikea_ingared_beige.rtm lib/monitor_lg_24m38h.rtm lib/mouse_microsoft_intellimouse_optical_usb.rtm lib/mousepad_generic.rtm lib/book_rendering_with_radiance.rtm lib/computer_em035_mecer_prelude_micro_atx_tower.rtm lib/earphones_sennheiser_m2_ieg.rtm lib/sconce_generic_sphere.rtm lib/door_double_sliding.rtm lib/switch_generic.rtm lib/door_single_820.rtm lib/window_generic.rtm lib/lamp_floor_generic.rtm lib/shoe_rack_generic.rtm lib/switch_toggle_generic.rtm lib/gpo_double.rtm lib/plug_generic.rtm lib/context.rtm
	rad -N 12 scene.rif

.PHONY : clean
clean:
	rm scene.oct scene.amb
