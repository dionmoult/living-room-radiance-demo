# living-room-radiance-demo

This project is a demo of Radiance in use to render a photorealistic scene. The
objectives are:

 - Full open-source pipeline
 - Geometry-heavy, complex scene
 - Uses CG techniques for texturing
 - Can generate an image that people think is a photo
 - No artistic interpretation of materials or light definitions
 - Can simulate how the human eye sees the space
 - Scene shown under night, day, artifical and natural light
 - Mix of real and fake objects
